<?php
/**
 * 订单服务
 * Enter description here ...
 * @author Wolf
 *
 */
class OrderService {
	
	const SUCCESS = 'success';
	const ERROR = 'error';
	//订单一页显示条数
	private $_num = 40;
	
	private $_chargeType = array ("充值" => 0, "消费" => 1, "赠送" => 2 );
	private $_payment = array ("收入" => 0, "支出" => 1 );
	private $_status = array ("全部" => "0", "等待审核" => 1, "成功" => 8, "关闭" => - 1 );
	//产品库
	private $_GoodsObj;
	
	public function __construct(IGoods $goodsClass) {
		$this->_GoodsObj = $goodsClass;
	}
	
	public function listing($type, $value, $p) {
		
		if (! empty ( $value )) {
			$where = array ($type => trim ( $value ) );
		} else {
			$where = null;
		}
		$totalNum = OrderModel::instance ()->getOrderTotalNum ();
		$page = $this->page ( $totalNum ['num'], $p, $this->_num );
		$rs = OrderModel::instance ()->orderPage ( $where, $page ['start'], $page ['num'] );
		$status = array_flip ( $this->_status );
		
		foreach ( $rs as $k => $v ) {
			$rs [$k] ['status'] = $status [$v ['status']];
		}
		return array ('list' => $rs, 'totalnum' => $totalNum ['num'], 'page' => $page, 'status' => $this->_status );
	}
	
	public function search($name) {
		$buy = new BuyService ();
		$name = urldecode ( $name );
		$rs = $buy->getGoodsLikeTitle ( $name );
		if (empty ( $rs )) {
			return;
		}
		$filter = array ();
		$i = 0;
		foreach ( $rs as $k => $v ) {
			if ($v ['cid'] != 2097) {
				continue;
			}
			$i ++;
			$filter [$i] = $v;
		}
		return $filter;
	}
	
	public function getSalesTopGoods() {
		return BuyModel::instance ()->getGoodsByCid ( 0, 20, 2097 );
	}
	
	public function status($orderno, $status) {
		//检查当前状态
		$order = $this->getOrderByOrderno ( $orderno );
		
		//订单为成功或者失败后就不允许修改
		if ($order ['status'] == $this->_status ['成功'] || $order ['status'] == $this->_status ['失败']) {
			return "订单无法修改";
		}
		
		//如果status为关闭 就归还积分
		OrderModel::instance ()->saveStatus ( array ('status' => $status, 'pay_time' => time () ), array ('orderno' => $orderno ) );
		
		return self::SUCCESS;
	}
	
	public function getOrderByOrderno($orderno) {
		$order = OrderModel::instance ()->getOrderByWhere ( array ('orderno' => $orderno ) );
		return $order [0];
	}
	
	public function getUserOrder($uid, $p) {
		
		if (! isset ( $uid )) {
			return "请先登录";
		}
		
		$where = array ('uid' => $uid );
		$totalNum = OrderModel::instance ()->countOrder ( $where );
		$page = $this->page ( $totalNum ['num'], $p, $this->_num );
		$rs = OrderModel::instance ()->orderPage ( $where, $page ['start'], $page ['num'] );
		
		$status = array_flip ( $this->_status );
		//匹配图片
		foreach ( $rs as $k => $v ) {
			$rs [$k] ['status'] = $status [$v ['status']];
			$rs [$k] ['goodslist'] = OrderModel::instance ()->getGoodsInfo ( $v ['orderno'] );
			$rs [$k] ['goodsnum'] = count ( $rs [$k] ['goodslist'] );
		}
		
		return array ('list' => $rs, 'page' => $page, 'totalnum' => $totalNum ['num'] );
	}
	
	/**
	 * 处理购物车信息与产品之间的关系 修正数量为0 不显示
	 * @param array $shopCart
	 */
	public function parseShopCart($shopCart) {
		$data = json_decode ( $shopCart );
		$info = array ();
		$i = 0;
		$totalcount = 0;
		foreach ( ( array ) $data as $k => $v ) {
			
			$v = ( array ) $v;
			if ($v ['count'] < 1) {
				continue;
			}
			//引入产品类
			$goodsinfo = $this->_GoodsObj->getGoodsById ( $k );
			$info [$i] ['id'] = $k;
			$info [$i] ['image'] = $goodsinfo ['thumb'];
			$info [$i] ['count'] = $v ['count'];
			$info [$i] ['name'] = $v ['name'];
			$info [$i] ['price'] = $v ['price'];
			$i ++;
			$totalcount += $v ['count'] * $v ['price'];
		}
		return array ('goodsinfo' => $info, 'totalcount' => $totalcount );
	}
	
	/**
	 * 打印未确认的订单
	 */
	public function p() {
		$verifyOrder = OrderModel::instance ()->getVerifyOrder ();
		$this->setCsvHeader ();
		echo "序号,收货人,区域,军区,商品名称,单价,数量,总额,备注";
		echo "\n";
		$area = new MemberAreaModule ();
		foreach ( $verifyOrder as $v ) {
			$dq = $area->getCon ( $v ['uid'] );
			//备注中不能存在,号 和换行
			$remark = str_replace ( ",", "", $v ['remark'] );
			$remark = str_replace ( "<br>", "", $remark );
			echo sprintf ( "%s,%s,%s,%s,%s,%s,%s,%s,%s", $v ['id'], $v ['shr'], $dq ['junqu'], $dq ['land'], $v ['goods_name'], $v ['coupons'], $v ['num'], $v ['coupons_total'], $remark );
			echo "\n";
		}
	}
	private function setCsvHeader() {
		header ( "Cache-Control: public" );
		header ( "Pragma: public" );
		header ( "Content-type:application/vnd.ms-excel" );
		$file = date ( "md", time () );
		header ( "Content-Disposition:attachment;filename=$file.csv" );
	}
	/**
	 * 批量打印并且合并订单 可以直接修改返回值
	 * */
	private function getVerifyOrder() {
		return OrderModel::instance ()->getVerifyOrder ();
	}
	/**
	 * 分页
	 *
	 * @return Array
	 */
	private function page($total, $pageid, $num) {
		$pageid = isset ( $pageid ) ? $pageid : 1;
		$start = ($pageid - 1) * $num;
		$pagenum = ceil ( $total / $num );
		/*修正分类不包含内容 显示404错误*/
		$pagenum = $pagenum == 0 ? 1 : $pagenum;
		/*如果超过了分类页数 404错误*/
		
		if ($pageid > $pagenum) {
			return false;
		}
		
		$page = array ('start' => $start, 'num' => $num, 'current' => $pageid, 'page' => $pagenum );
		return $page;
	}
	
	/**
	 * 获取产品信息
	 * @return 订单名称 总价  产品件数  产品列表
	 */
	public function getGoodsInfo($data, $sno) {
		$moneyTotal = 0;
		$couponsTotal = 0;
		$goodsArr = array ();
		$num = 0;
		$title = "";
		$buy = new BuyService ();
		$buy->_cookie = 'jsorder';
		$data = $buy->getCart ( 2 );
		foreach ( $data ['goods'] as $k => $v ) {
			if ($num == 0) {
				$title = $v ['goods_name'];
			}
			$goods = array ('orderno' => $sno, 'goods_id' => $v ['sku'], 'num' => $v ['num'] * $v ['count'], 'goods_name' => $v ['goods_name'], 'prices' => 0, 'coupons' => $v ['price'] * 100, 'money' => $v ['price'] * $v ['count'] * 100 * $v ['num'], 'coupons_total' => $v ['price'] * 100 * $v ['count'] * $v ['num'] );
			$moneyTotal += $goods ['money'];
			$couponsTotal += $goods ['coupons_total'];
			$goodsArr [] = $goods;
			$num += $v ['num'];
		}
		return array ('name' => $title, 'moneytotal' => $moneyTotal, 'couponstotal' => $couponsTotal, 'goodslist' => $goodsArr, 'num' => $num );
	}
	
	/**
	 * 
	 * 添加订单
	 * @param String $sno
	 * @param String $name 订单名称
	 * @param int $uid
	 * @param String $realname
	 * @param String $shr
	 * @param String $address
	 * @param String $mobile
	 * @param String $num
	 * @param decimal $coupons
	 * @param decimal $money
	 * @param String $remark
	 */
	public function addOrder($sno, $name, $uid, $realname, $shr, $address, $mobile, $num, $coupons, $money, $remark, $status) {
		$params = array ('uid' => $uid, 'name' => $name, 'orderno' => $sno, 'shr' => $shr, 'address' => $address, 'mobile' => $mobile, 'addtime' => time (), 'remark' => $remark, 'num' => $num, 'money' => $money, 'coupons' => $coupons, 'status' => $status );
		return OrderModel::instance ()->addOrder ( $params );
	
	}
	/**
	 * 增加订单明细  蒋getGoodsInfo中的goodsList 传递进来即可
	 * @param unknown_type $goodsList
	 */
	public function addGoods($goodsList) {
		foreach ( $goodsList as $v ) {
			OrderModel::instance ()->addGoods ( $v );
		}
		return true;
	}
	private function object_array($array) {
		if (is_object ( $array )) {
			$array = ( array ) $array;
		}
		if (is_array ( $array )) {
			foreach ( $array as $key => $value ) {
				$array [$key] = $this->object_array ( $value );
			}
		}
		return $array;
	}
	/**
	 * 获取产品扩展属性  产品价格和积分 代理价格 产品件数 销售
	 *
	 */
	private function getGoodsPrices($gid) {
		//获取价格字段
		$rs = $this->_GoodsObj->getGoodsById ( $gid );
		if (empty ( $rs ))
			return;
		$coupons = $this->_GoodsObj->getGoodsPriceById ( $gid );
		if (! empty ( $coupons )) {
			$rs ['coupons'] = $coupons [0] ['value'];
		} else {
			$rs ['coupons'] = 100000;
		}
		return $rs;
	}
}