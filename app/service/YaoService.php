<?php

class YaoService
{

    private $_num = 2; // 每人最多2次
    public function add ($openid)
    {
        $num = $this->getNumByOpenId($openid);
        if ($num >= 2) {
            return array(
                    'status' => false,
                    'message' => "加油"
            );
        }
        
        $weixin = new WeixinService();
        // 如果已经存在了openid
        $isUserExsit = $this->getUserByOpenId($openid);
        if (empty($isUserExsit)) {
            $wuser = $weixin->getUserByOpenid($openid);
        } else {
            $wuser = $isUserExsit;
        }
        
        $user = array(
                'openid' => $wuser['openid'],
                'headimgurl' => $wuser['headimgurl'],
                'nickname' => $wuser['nickname'],
                'add_time' => time()
        );
        $id = $this->addUser($user);
        if ($id <= 0) {
            return array(
                    'status' => false,
                    'message' => "失败"
            );
        } else {
            return array(
                    'status' => true,
                    'message' => $id,
                    'data' => $id
            );
        }
    }

    public function cleanList ()
    {
        return YaoModel::instance()->cleanList();
    }

    public function getUserMaxId ($id)
    {
        $rs = YaoModel::instance()->getUserMaxId($id);
        if (empty($rs)) {
            
            return array(
                    'status' => false,
                    'message' => "赶紧啦"
            );
        } else {
            
            return array(
                    'status' => true,
                    'message' => "加油",
                    'data' => $rs
            );
        }
    }

    public function getUserByOpenId ($openid)
    {
        return YaoModel::instance()->getUserByOpenId($openid);
    }

    public function addUser ($openid)
    {
        return YaoModel::instance()->addList($openid);
    }

    public function getNumByOpenId ($openid)
    {
        return YaoModel::instance()->getNumByOpenid($openid);
    }
}

class YaoModel extends Db
{

    private $_yao_list = 'd_yao_list';

    public function addList ($params)
    {
        return $this->add($this->_yao_list, $params);
    }

    public function cleanList ()
    {
        $sql = "truncate $this->_yao_list";
        return $this->exec($sql);
    }

    public function getUserMaxId ($id)
    {
        $sql = "SELECT * FROM $this->_yao_list WHERE id>$id order by id ASC LIMIT 1";
        return $this->fetch($sql);
    }

    public function getUserByOpenId ($openid)
    {
        return $this->getOne($this->_yao_list, 
                array(
                        'openid' => $openid
                ));
    }

    public function getNumByOpenid ($openid)
    {
        return $this->getNum($this->_yao_list, 'id', 
                array(
                        'openid' => $openid
                ));
    }

    /**
     *
     * @return YaoModel
     */
    public static function instance ()
    {
        return parent::_instance(__CLASS__);
    }
}
