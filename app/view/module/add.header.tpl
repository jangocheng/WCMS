<script type="text/javascript"
	src="./static/public/jquery-1.11.0.min.js"></script>


<script src="./static/public/jquery-ui.js" type="text/javascript"
	charset="utf-8"></script>


<!-- 编辑器源码文件 -->
<!-- 语言包文件(建议手动加载语言包，避免在ie下，因为加载语言失败导致编辑器加载失败) -->
<script src="./static/public/evol.colorpicker.min.js"
	type="text/javascript" charset="utf-8"></script>
<link href="./static/public/css/evol.colorpicker.css" rel="stylesheet"
	type="text/css">
<link href="./static/public/css/jquery-ui.css" rel="stylesheet"
	type="text/css">
<link href="./static/bootstrap2/css/bootstrap-datetimepicker.min.css"
	rel="stylesheet" media="screen">

<script type="text/javascript"
	src="./static/bootstrap2/js/bootstrap-datetimepicker.js"
	charset="UTF-8"></script>
<script type="text/javascript"
	src="./static/bootstrap2/js/bootstrap-datetimepicker.zh-CN.js"
	charset="UTF-8"></script>
<script type="text/javascript" src="./static/public/layer/layer.min.js"></script>



{literal}
<style>
.table th,.table td {
	border-top: none;
	border-bottom: 1px solid #f1f1f1;
}
</style>
<script type='text/javascript'>
	$(document)
			.ready(
					function() {
						$('#cate')
								.change(
										function() {
											var a = $(this).children(
													'option:selected').val(); //弹出select的值
											$
													.post(
															"./index.php?factory/extend",
															{
																cid : a
															},
															function(data) {
																var str;
																var sub = $(
																		"#submit")
																		.html();
																for ( var i = 0; i < data.data.length; i++) {

																	str += "<tr><td class=\"span2\">"
																			+ data.data[i].name
																			+ "</td><td class=\"span8\"><input type=\"text\" class=\"input-xlarge\" name=\""+data.data[i].key+"\">"
																			+ data.data[i].key
																			+ "|"
																			+ data.data[i].type
																			+ " </td><td></td></tr>";

																}
																str = str
																		+ "<tr>"
																		+ sub
																		+ "</tr>";
																$("#extend")
																		.html(
																				str);
															}, "json")
										});

						$("#mycolor").colorpicker({
							color : "#ffc000",
							history : false,
							displayIndicator : false
						});
						$("#mycolor")
								.on(
										"change.color",
										function(event, color) {
											$('#title').css('color', color);
											var x = $("#b").attr("class");
											if (x == "b") {
												$("input[name='css']")
														.val(
																'color:'
																		+ color
																		+ ";font-weight:bold;");
											} else {
												$("input[name='css']").val(
														'color:' + color);
											}
										})
					});

	function checktitle() {
		var t = $("#title").val();

		$.post("./index.php?factory/search", {
			key : "title",
			value : t,
			datatype : "json"
		}, function(result) {
			if (result.status == "success") {
				$("#checktitle").addClass("alert alert-success");
			} else {
				$("#checktitle").addClass("alert alert-error");
			}
			var str = "<ul>";
			for ( var i = 0; i < result.data.length; i++) {
				str += "<li>" + result.data[i].title + "</li>";
			}
			str = str + "</ul>";
			$("#checktitle").html(str)
		}, "json")
	}

	function apd(t) {
		var htm = '<input type="text" name='+t+'[]>';
		$("#" + t).append(htm);
	}
	function jiacu() {
		var c = $("#mycolor").val();
		if (c != "#ffc000") {
			c = "color:" + c + ";";
		} else {
			c = "";
		}

		var x = $("#b").attr("class");
		if (x == "b") {
			$("#title").css("font-weight", "");
			$("#b").removeClass("b");
			$("input[name='css']").val(c);
		} else {
			$("#title").css("font-weight", "bold");
			$("#b").addClass("b");
			$("input[name='css']").val(c + "font-weight:bold;");
		}

	}
</script>
{/literal} {include file="news/top.tpl"} {include file="news/nav.tpl"}


<!-- start: Content -->
<div id="content">


	<div class="row-fluid">



		<div class="well">
			<!-- Only required for left/right tabs -->
			<ul class="nav nav-tabs suoding">
				<li><a href="javascript:history.go(-1)">« 返回</a></li>

				<li class="active"><a href="#tab1" data-toggle="tab">基本</a></li>
				<li><a href="#tab2" data-toggle="tab">权限</a></li>
				<li><a href="#tab3" data-toggle="tab">扩展</a></li>

			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab1">

					<form name="news" action="./index.php?factory/add" method="post"
						enctype="multipart/form-data" class="form-inline">
						<!-- 内容// -->
						<input type="hidden" name="repeat" value="{$repeat}">
						<table class="table">

							<tr>
								<td class="span2">标题</td>
								<td class="span8"><input type="hidden" name="css" value="" />
									<span class="input-append"> <input type="text"
										name="title" class="input-xxlarge rule" id="title"> <a
										class="btn " href="javascript:checktitle()">重复</a>
										<a class="btn " href="javascript:jiacu();"><i
											class="icon-bold"></i></a> <a class="btn "><input
											type="hidden" id="mycolor" class="colorPicker evo-cp0" /></a>

								</span>

									<div id="checktitle"></div></td>
								<td>

									<div id="b"></div>

								</td>

							</tr>


							<tr>
								<td class="span2">副标题</td>
								<td class="span8"><input type="text" name="subtitle"
									class="input-xlarge" id="title">


									<div id="checktitle"></div></td>
								<td></td>

							</tr>

							<tr>
								<td></td>
								<td>
									<div class="col-md-4">{$category}</div>
								</td>
								<td></td>

							</tr>


							<tr>
								<td>标签</td>
								<td>{foreach from=$flag item=g} {$g.name}: {foreach
									from=$g.data item=l} <label class="checkbox"> <input
										type="checkbox" value="{$l.id}" name="flag[]">{$l.name}
								</label> {/foreach} <br> {/foreach}
								</td>
								<td></td>

							</tr>

							<tr>
								<td>缩略图</td>
								<td>
									<div class="uploader" id="uniform-fileInput">
										<input class="input-file uniform_on" name="thumb"
											id="fileInput" type="file"><span class="filename"
											style="-webkit-user-select: none;">No file selected</span><span
											class="action" style="-webkit-user-select: none;">上传</span>
									</div> <input type="text" size="3" maxlength="3" name="width"
									class="input-mini"
									value="{if $width!=''}{$width}{else}360{/if}">x<input
									maxlength="3" type="text" size="3" name="height"
									class="input-mini"
									value="{if $height!=''}{$height}{else}360{/if}"> 或 <label
									class="checkbox"> <input type="checkbox" name="same">原图
								</label>
								</td>
								<td></td>

							</tr>