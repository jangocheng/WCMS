<?php
/**
 * 订单系统  属于半独立系统，权限方面不考虑
 * 2013-2-27
 * @author wolf Email: 116311316@qq.com
 *
 */
class OrderController extends NodeController {
	//以下三个参数对应的是积分记录 而不是订单
	private $chargeType = array ("充值" => 0, "消费" => 1, "赠送" => 2 );
	private $payment = array ("收入" => 0, "支出" => 1 );
	private $status = array ("全部" => "0", "等待审核" => 1, "成功" => 8, "关闭" => - 1 );
	private $_pageNum = 20; //分页订单显示条数
	static $orderService;
	
	//添加订单 采用了cookie
	public function add() {
		$rs = $this->create ();
		
		$this->sendNotice ( $rs ['message'], null, $rs ['status'] );
	}
	
	public function cart() {
		//刷新购物车
		$buy = new BuyService ();
		$buy->_cookie = 'jsorder';
		$rs = $buy->getCart ( 1 );
		self::getLogService ()->add ( $this->_user_global ['real_name'], "查看购物车" );
		
		$total = $rs ['money1'] + $rs ['money2'];
		$this->view ()->assign ( "goods", $rs ['goods'] );
		$this->view ()->assign ( "totalcount", $total );
		
		$this->view ()->display ( "file:order/cart.tpl" );
	}
	
	public function iframe() {
		$vistor = self::getLogService ()->getLogByEvent ( "积分商城", 5 );
		$recommed = self::getOrderService ()->getSalesTopGoods ();
		$this->view ()->assign ( 'recommend', $recommed );
		$this->view ()->assign ( 'vistor', $vistor );
		$this->view ()->display ( "file:order/iframe.tpl" );
	}
	
	/**
	 * 对接购物车和产品库  创建订单和用户账户信息
	 */
	public function create() {
		//获取用户信息
		if (empty ( $this->_user_global )) {
			return array ('status' => false, 'message' => "请先登录" );
		}
		
		$user = $this->_user_global;
		
		//获取提交的订单信息 过滤多余字段
		$remark = trim ( $_POST ['remark'] );
		//获取提交的信息 只要产品id和产品数量 
		//获取产品
		$goods = $this->getGoods ();
		if (empty ( $goods ['goods'] )) {
			return array ('status' => false, 'message' => "购物车是空的" );
		
		}
		
		$data = array ();
		
		//判断产品类型
		//判断用户积分或者金额是否足够
		//获取sno号
		$rs = OrderModel::instance ()->getSno ();
		$time = time ();
		//创建订单号
		$sno = $rs ['value'] . date ( 'YmdHis', $time );
		$ginfo = self::getOrderService ()->getGoodsInfo ( $data, $sno );
		if ($user ['coupons'] < $ginfo ['couponstotal']) {
			self::getLogService ()->add ( $this->_user_global ['username'], "创建订单失败:积分不够" );
			return array ('status' => false, 'message' => "积分不够" );
		}
		
		//增加积分记录
		$id = self::getCouponsService ()->addCouponsHistory ( $user ['uid'], $user ['real_name'], - $ginfo ['couponstotal'], $ginfo ['name'], $this->payment ["支出"], $this->chargeType ["消费"], $sno, $this->status ['等待审核'] );
		
		if ($id > 0) {
			$coupons = $user ['coupons'] - $ginfo ['couponstotal'];
			self::getMemberService ()->saveCoupons ( $user ['uid'], $ginfo ['couponstotal'], 1 );
		} else {
			self::getLogService ()->add ( $this->_user_global ['username'], "创建订单失败:积分历史无法记录" );
			$this->redirect ( self::ERROR );
		}
		
		//订单列表  生成唯一sno号 判断库存是否有库存
		self::getOrderService ()->addOrder ( $sno, $ginfo ['name'], $user ['uid'], $user ['real_name'], $user ['real_name'], $user ['area'], $user ['mobile_phone'], $ginfo ['num'], $ginfo ['couponstotal'], $ginfo ['moneytotal'], $remark, $this->status ['等待审核'] );
		//订单明细
		self::getOrderService ()->addGoods ( $ginfo ['goodslist'] );
		//增加操作日志
		self::getLogService ()->add ( $this->_user_global ['username'], "创建订单成功$sno" );
		setcookie ( "jsorder", "", time () - 3600, "/" );
		return array ('status' => true, 'message' => "订单提交成功" );
	
	}
	
	private function getGoods() {
		$buy = new BuyService ();
		$buy->_cookie = 'jsorder';
		return $buy->getCart ( 2 );
	
	}
	
	/**
	 * 订单列表
	 * Enter description here ...
	 */
	public function listing() {
		// 默认设置为15条
		$rs = self::getOrderService ()->listing ( $_GET ['type'], $_GET ['value'], $_GET ['p'] );
		
		$this->view ()->assign ( 'num', $rs ['page'] );
		
		$this->view ()->assign ( 'totalnum', $rs ['totalnum'] );
		$this->view ()->assign ( 'news', $rs ['list'] );
		$this->view ()->assign ( 'status', $rs ['status'] );
		$this->view ()->display ( 'file:order/olist.tpl' );
	}
	
	/**
	 * 修改订单状态 和积分状态 ok
	 */
	public function setStatus() {
		
		$rs = self::getOrderService ()->status ( $_POST ['orderno'], $_POST ['status'] );
		
		if ($rs == self::SUCEESS) {
			$log = $_POST ['orderno'] . "修改状态为" . $_POST [status];
			
			self::getLogService ()->add ( $this->_user_global ['username'], $log );
			self::getCouponsService ()->status ( $_POST ['status'], $_POST ['orderno'] );
			//-1 为关闭订单 退回金额
			if ($_POST ['status'] == - 1) {
				
				$order = self::getOrderService ()->getOrderByOrderno ( $_POST ['orderno'] );
				$member = self::getMemberService ()->getMemberByUid ( $order ['uid'] );
				
				self::getMemberService ()->saveCoupons ( $order ['uid'], $order ['coupons'], 0 );
				$log = $_POST ['orderno'] . "退回" . $member [username] . "积分" . $order [coupons];
				
				self::getLogService ()->add ( $this->_user_global ['username'], $log );
			}
		
		}
		
		$this->sendNotice ( $rs );
	}
	
	/**
	 * 对接购物车 展示产品信息  订单确认
	 */
	public function confirm() {
		$cart = self::getOrderService ()->parseShopCart ( $_POST ['goodsinfo'] );
		$this->view ()->assign ( "totalcount", $cart ['totalcount'] );
		$this->view ()->assign ( "news", $cart ['goodsinfo'] );
		$this->view ()->display ( "file:order/confirm.tpl" );
	}
	
	public function shop() {
		$buy = new BuyService ();
		$buy->_cookie = 'jsorder';
		$rs = $buy->getGoodsByCid ( $_GET ['p'], 2097 );
		$this->view ()->assign ( 'cate', $rs ['cate'] );
		$this->view ()->assign ( 'num', $rs ['page'] );
		$this->view ()->assign ( 'goods', $rs ['goods'] );
		$this->view ()->display ( 'file:order/img.tpl' );
	}
	
	/*获取订单详情*/
	public function i() {
		$goods = OrderModel::instance ()->getGoodsBySNO ( trim ( $_GET ['sno'] ) );
		/*增加算法,代理价优先*/
		$order = OrderModel::instance ()->getOrderByWhere ( array ('orderno' => trim ( $_GET ['sno'] ) ) );
		$order = $order [0];
		$this->view ()->assign ( 'order', $order );
		$this->view ()->assign ( 'goods', $goods );
		$this->view ()->assign ( 'p', $_GET ['p'] );
		/*增加提示信息 是否多人同时操作*/
		$wl = NewsModel::instance ()->getNewsByWhere ( array ('cid' => 1079 ) );
		$this->view ()->assign ( 'wl', $wl );
		$this->view ()->display ( 'file:order/goods.tpl' ); /*产品详情页*/
	}
	
	public function p() {
		self::getOrderService ()->p ();
	}
	/**
	 * 
	 * 前端用户订单列表
	 */
	public function trade() {
		$rs = self::getOrderService ()->getUserOrder ( $this->_user_global ['uid'], $_GET ['p'] );
		$this->view ()->assign ( 'totalnum', $rs ['totalnum'] );
		$this->view ()->assign ( 'num', $rs ['page'] );
		$this->view ()->assign ( 'userinfo', $this->_user_global );
		$this->view ()->assign ( "order", $rs ['list'] );
		$this->view ()->display ( "file:order/trade.tpl" );
	}
	
	//前段产品搜索
	public function find() {
			$recommed = self::getOrderService ()->getSalesTopGoods ();
		$this->view ()->assign ( 'recommend', $recommed );
		$rs = self::getOrderService ()->search ( $_GET ['title'] );
		$title = urldecode ( $_GET ['title'] );
		self::getLogService ()->add ( $this->_user_global ['real_name'], "搜索$title" );
		$page = array ('num' => 50, 'current' => 1, 'page' => 1 );
		$this->view ()->assign ( 'num', $page );
		$this->view ()->assign ( 'goods', $rs );
		$this->view ()->display ( "file:order/img.tpl" );
	}
	
	/**
	 * 订单查询
	 */
	public function search() {
		$rs = self::getOrderService ()->search ( $_POST ['type'], $_POST ['value'] );
		$this->view ()->assign ( 'num', array ('num' => 20, 'current' => 1, 'page' => 1 ) );
		$this->view ()->assign ( 'totalnum', 1 );
		$this->view ()->assign ( 'news', $rs );
		$this->view ()->display ( 'file:order/olist.tpl' );
	}
	
	public static function getMemberService() {
		return new MemberService ();
	}
	
	public static function getCouponsService() {
		return new CouponsService ();
	}
	
	/**
	 * 获取服务累
	 * Enter description here ...
	 */
	public static function getOrderService() {
		if (self::$orderService == null) {
			self::$orderService = new OrderService ( new GoodsService () );
		}
		return self::$orderService;
	}

}