<?php
//这个版本最全的
$lang = array();
$lang['DIG'] = '顶';
$lang['DUGG'] = '已顶';
$lang['DIGIT'] = '顶一下';
$lang['JOIN'] = '加入';
$lang['LOGIN'] = '登录';
$lang['LOGOUT'] = '注销';
$lang['SIGNUP'] = '注册';
$lang['SUBMIT'] = '提交';
$lang['SEARCH'] = '搜索';
$lang['SAVE'] = '保存';
$lang['DEL'] = '删除';
$lang['MOVE'] = '移动';
$lang['CREATE']='创建';
$lang['PUB']='发布';
$lang['COPY'] = '复制';
$lang['PASS'] = '通过';
$lang['REFUSE'] = '拒绝';
$lang['SELECT'] = '选择';
$lang['SELECTALL'] = '全选';
$lang['RECYCLE'] = '回收站';
$lang['PREVIOUS'] = '预览';
$lang['UPLOAD'] = '上传';
$lang['RESET'] = '重置';
$lang['CHECK'] = '验证';
$lang['AUTHORIZE'] = '授权';
$lang['BIND'] = '绑定';
$lang['CODEIMG']='验证码';
$lang['UPDATE'] = '更新';
$lang['SHIMING'] = '实名';


//系统后台内容 回复
$lang['BASE']='基本';
$lang['ADDIT']='辅助';
$lang['PRODUCT']='产品';
$lang['DIRECT']='描述';
$lang['AREA']='地区';

$lang['SHOW']='显示';
$lang['HIDDEN']='隐藏';
$lang['WEIGHT']='权重';
$lang['TITLECOLOR']='标题颜色';
$lang['VIEWS']='点击';
$lang['ID'] = '编号';
$lang['TITLE'] = '主题';
$lang['SUBTITLE'] = '副标题';
$lang['KEYWORD'] = '关键词';
$lang['CATE'] = '栏目';
$lang['PUBTIME'] = '发布时间';
$lang['AUTHOR'] = '作者';
$lang['ACTION'] = '操作';
$lang['SORT'] = '排序';
$lang['MOBILE'] = '手机';
$lang['USERNAME'] = '姓名';
$lang['CONTENT'] = '内容';
$lang['RANGE'] = '范围';
$lang['TYPES'] = '类型';
$lang['NOREAD'] = '未读';
$lang['READ'] = '已读';
$lang['COUNTRY']='国家';
$lang['CITY']='城市';
$lang['EMAIL']='邮箱';
$lang['STATUS']='状态';
$lang['FORBIDDEN']='禁用';
$lang['SUMMARY']='摘要';
$lang['THUMB']='缩略图';
$lang['ALL']='全部';
$lang['PREMISSION']='权限';
$lang['FLAG']='标记';
$lang['PIC']='原图';
$lang['ADDNOTICE']='勾选原图，所填值无效';
$lang['EXTEND']='扩展字段';
$lang['TOOL']='工具';
$lang['GROUP']='用户组';
$lang['LOGINTIME']='登录时间';
$lang['ACCOUNT']='账号';
$lang['REMARK']='备注';
$lang['PASSWORD'] = '密码';
$lang['COUPONS'] = '积分';
$lang['SPECPAGE'] = '专题页';
$lang['LISTPAGE'] = '列表页';
$lang['CONTENTPAGE'] = '内容页';
$lang['CURRENT'] = '当前';
$lang['NAME'] = '名称';
$lang['VALUE'] = '值';
$lang['STATIC'] = '静态';
$lang['SUCCESS'] = '成功';
$lang['FAILED'] = '失败';
$lang['FORWARD']='继承';
$lang['NUMBER']='序号';

$lang['MANAGER']='权限组';

//系统配置信息
$lang['sys']['sort'] = '排序';
$lang['sys']['water'] = '水印位置';
$lang['sys']['pagenum'] = '新闻条数';
$lang['sys']['newsnum'] = '分类条数';
$lang['sys']['smarty'] = 'Smarty';
$lang['sys']['sys'] = '系统';
$lang['sys']['php'] = 'php';
$lang['sys']['cgi'] = '运行方式';
$lang['sys']['timezone'] = '时区';
$lang['sys']['mysql'] = 'mysql';

//系统后台菜单语言
$lang['news']['listing'] = '内容列表';
$lang['news']['config'] = '系统配置';
$lang['news']['addcon'] = '添加内容';
$lang['comment']['clist'] = '回复列表';
$lang['member']['loginlog'] = '登录日志';
$lang['member']['add'] = '新增会员';
$lang['member']['repassword'] = '修改密码';
$lang['attr']['attr'] = '数据字典';
$lang['attr']['moreattr'] = '多值字段';
$lang['attr']['bindcate'] = '绑定分类';
$lang['attr']['flag'] = '定义标签';
$lang['temp']['templist'] = '模板库';
$lang['member']['listing'] = '会员列表';
$lang['cate']['editcategory'] = '栏目管理';
$lang['file']['index'] = '文件管理';
$lang['order']['listing'] = '订单列表';
$lang['news']['batchall'] = '全站静态';
$lang['vote']['config'] = '投票设置';
$lang['member']['facelist'] = '头像审核';

//前台导航
$lang['NAV_INDEX'] = '首页';
$lang['NAV_PRODUECT'] = '产品目录';
$lang['NAV_ABOUT'] = '关于我们';
$lang['NAV_CER'] = '证书';
$lang['NAV_CONTACT'] = '联系我们';
//前台留言表单
$lang['FORM_USERNAME']='联系人';
$lang['FORM_MOBILE']='手机';
$lang['FORM_EMAIL']='Email';
$lang['FORM_CONTENT']='内容';

